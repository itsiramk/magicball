# magic_ball

This project demonstrates the use of both Stateless and Stateful widgets in flutter.

It contains a Scaffold to display title with backgroundColor.

It uses Random function to change images being displayed from the Asset images.

Also uses String interpolation to change images using TextButton widgets onPressed() method.

![magicball](images/magicball.PNG)
